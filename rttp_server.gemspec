lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rttp_server/version'

@source_code_uri = 'https://gitlab.com/danielfoord/rttp_server'
@changelog_uri = 'https://gitlab.com/danielfoord/rttp_server/CHANGELOG.md'

def setup_push(spec)
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'Set to \'http://mygemserver.com\''

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = @source_code_uri
    spec.metadata['changelog_uri'] = @changelog_uri
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end
end

Gem::Specification.new do |spec|
  spec.name = 'rttp_server'
  spec.version = RttpServer::VERSION
  spec.authors = ['Daniel Foord']
  spec.email = ['daniel.foord@agrigate.co.za']

  spec.summary = 'Static HTTP file server'
  spec.description = 'Static HTTP file server'
  spec.homepage = 'https://gitlab.com/danielfoord/rttp_server'

  setup_push(spec)

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir = 'exe'
  spec.executables = ['rttp_server']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rerun', '~> 0.13.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.63.1'
end
