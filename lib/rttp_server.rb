#!/usr/bin/env ruby

require 'sinatra/base'
require 'json'
require 'optparse'
require_relative 'rttp_server/version'
require_relative 'rttp_server/integer'
require_relative 'rttp_server/map_item_helper'
require_relative 'rttp_server/parse_options_extension'

# RttpServer module
module RttpServer
  # Main Sinatra Conroller
  class ApplicationController < Sinatra::Base
    register Sinatra::ParseOptionsExtension
    helpers Sinatra::MapItemHelper

    options = {}
    options = parse_options if $PROGRAM_NAME == __FILE__

    set :bind, '0.0.0.0'
    set :root, File.dirname(__FILE__)
    set :args, options
    set :version, RttpServer::VERSION
    set :views, File.join(root, 'views')
    set :static, File.join(root, 'public')

    use Rack::Auth::Basic, 'Protected Area' do |username, password|
      username == settings.args[:username] && password == settings.args[:password]
    end

    not_found do
      status 404
      erb :four_oh_four
    end

    get '/logout' do
      halt 401, erb(:logout)
    end

    get '/' do
      erb :index, locals: {
        contents: Dir
          .entries(settings.args[:dir])
          .reject { |p| ['.', '..'].include?(p) }
          .sort
          .map { |p| map_item("#{settings.args[:dir]}/#{p}") }
          .reject { |item| item[:is_directory] == false && item[:is_file] == false }, # Broken links
        version: settings.version,
        directory: settings.args[:dir]
      }
    end

    get(/#{settings.args[:dir]}(.*)/) do
      path = File.expand_path(CGI.unescape(params[:captures].first)).to_s
      path = "#{settings.args[:dir]}#{path}" if $PROGRAM_NAME == __FILE__

      if File.directory? path
        # Get all entries excluding reference to self
        entries = Dir.entries(path).reject { |p| p == '.' }
        # Remove reference to parent if we are in the root
        entries = entries.reject { |p| p == '..' } if path.chomp('/') == settings.args[:dir]

        entries = entries
                  .sort
                  .map { |p| map_item("#{path}/#{p}") }
                  .reject { |item| item[:is_directory] == false && item[:is_file] == false } # Broken links

        return erb :index, locals: {
          contents: entries,
          version: settings.version,
          directory: path
        }
      end

      if /^#{settings.args[:dir]}/ =~ path && File.exist?(path)
        content_type 'video/webm' if File.extname(path) == '.mkv'
        send_file path
      else
        puts 'WOT M8'
        halt 404
      end
    end
  end
end

RttpServer::ApplicationController.run! if $PROGRAM_NAME == __FILE__
