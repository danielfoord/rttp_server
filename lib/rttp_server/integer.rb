# Integer extensions
class Integer
  def to_filesize
    {
      'bytes' => 1024,
      'kB' => 1024 * 1024,
      'MB' => 1024 * 1024 * 1024,
      'GB' => 1024 * 1024 * 1024 * 1024,
      'TB' => 1024 * 1024 * 1024 * 1024 * 1024
    }.each_pair { |e, s| return format_filesize(e, s) if self < s }
  end

  private

  def format_filesize(symbol, amount)
    "#{(to_f / (amount / 1024)).round(2)} #{symbol}"
  end
end
