# Sinatra
module Sinatra
  # Sinatra helper to map directory entries
  module MapItemHelper
    def map_item(path)
      {
        url: CGI.escape(path).gsub('%2F', '/'),
        name: File.basename(path),
        is_file: File.file?(path),
        is_directory: File.directory?(path),
        item_count: File.directory?(path) ? "#{Dir.entries(path).select { |p| p != '.' && p != '..' }.length} items" : 0,
        file_size: File.file?(path) ? File.size(path).to_filesize : 0
      }
    end
  end

  helpers MapItemHelper
end
