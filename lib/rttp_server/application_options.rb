require 'optparse'

# RttpServer module
module RttpServer
  # Options that the application accepts
  class ApplicationOptions
    def parse
      options = {}
      OptionParser.new(ARGV) do |opts|
        opts.banner = 'Usage: rttp_server [options]'
        opts.on('-d', '--dir {directory}', 'The directory which to host', String) do |directory|
          options[:dir] = directory
        end

        opts.on('-u', '--usr {username}', 'The username of which to authenticate against', String) do |username|
          options[:username] = username
        end

        opts.on('-x', '--pwd {password}', 'The password of which to authenticate against', String) do |password|
          options[:password] = password
        end

        opts.on('-p', '--port {port}', 'The port to run the server on', String) do |port|
          options[:port] = port
        end
      end.parse!
      options
    end
  end
end
