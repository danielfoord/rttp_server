require 'optparse'
require_relative 'application_options'

# Sinatra module
module Sinatra
  # Parse options helper
  module ParseOptionsExtension
    def parse_options
      RttpServer::ApplicationOptions.new.parse
    end
  end

  register ParseOptionsExtension
end
